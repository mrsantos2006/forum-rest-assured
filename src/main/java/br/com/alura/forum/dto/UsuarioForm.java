package br.com.alura.forum.dto;

public class UsuarioForm {
	
	private String email;
	private String senha;
	
	public UsuarioForm(String email, String senha) {
		this.email = email;
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public String getSenha() {
		return senha;
	}

}
