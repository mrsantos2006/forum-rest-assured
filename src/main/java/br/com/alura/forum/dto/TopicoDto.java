package br.com.alura.forum.dto;

import java.time.LocalDateTime;


public class TopicoDto {
	
	private Long id;
	private String titulo;
	private String mensagem;
	private String dataCriacao;
	
	public TopicoDto() {}
	
	public Long getId() {
		return id;
	}
	public String getTitulo() {
		return titulo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public String getDataCriacao() {
		return dataCriacao;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public void setDataCriacao(String dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@Override
	public String toString() {
		return "TopicoDto [id=" + id + ", titulo=" + titulo + ", mensagem=" + mensagem + ", dataCriacao=" + dataCriacao
				+ "]";
	}
	
	

}
