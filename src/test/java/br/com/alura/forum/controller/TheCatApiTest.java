package br.com.alura.forum.controller;

import static io.restassured.RestAssured.DEFAULT_BODY_ROOT_PATH;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import java.util.List;

import org.junit.Test;

import io.restassured.http.ContentType;

public class TheCatApiTest {
	
	@Test
	public void deveRetornarListaDeVotosNaoVazia() {
		
		 List<Object> list =
		
		given()
			.header("x-api-key", "2dcb85b5-1388-492a-a99d-3cf0bb4fa3ec")
			.accept(ContentType.JSON)
		.expect()
			.statusCode(200)
		.when()
			.get("https://api.thecatapi.com/v1/votes")
			.andReturn()
			.jsonPath()
			.getList(DEFAULT_BODY_ROOT_PATH)
			;

		 assertThat(list.size(), greaterThanOrEqualTo(1));		 
		
	}
	
	@Test
	public void deveRetornarHeader() {
		
		 given()
			.header("x-api-key", "2dcb85b5-1388-492a-a99d-3cf0bb4fa3ec")
			.accept(ContentType.JSON)
		.expect()
			.statusCode(200)
		.expect()
			.header("Server", "nginx/1.18.0" )
		.when()
			.get("https://api.thecatapi.com/v1/votes")
			;
		 
		
	}

}
