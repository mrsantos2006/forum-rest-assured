package br.com.alura.forum.controller;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import org.junit.Test;

import br.com.alura.forum.dto.TopicoDto;
import br.com.alura.forum.dto.UsuarioForm;

public class TopicosTeste {

	@Test
	public void deveRetornarlistaComTresTopicos() {

		List<TopicoDto> list = get("/topicos")
			.andReturn()
			.jsonPath()
			.getList("content", TopicoDto.class);
		
		assertThat(list, hasSize(3));

	}
	
	@Test
	public void deveFiltrarTopicoComCursoHTML5() {
		
		given()
			.param("nomeCurso", "HTML 5")
		.when()
			.get("/topicos")
		.then()
			.body("content.size()", equalTo(1))
			.and()
			.body("content[0].titulo", equalTo("Duvida 3"))
			.and()
			.body("content[0].mensagem", equalTo("Tag HTML"))
			;

	}
	
	@Test
	public void deveConterItemComTituloDuvida() {
					given()
					.expect()
						.statusCode(200)
					.when()
						.get("/topicos")
					.then()
						.body("content.findAll { it.id < 7 }.titulo ", hasItems("Dvida 2", "Dvida"));
	}
	
	@Test
	public void deveRetornarListaVazia() {
		
		given()
			.param("nomeCurso", "Batatinha Quando Nasce Esperrama Pelo Chao")
		.when()
			.get("/topicos")
		.then()
			.body("content.size()", equalTo(0));
		
		
	}
	
	@Test
	public void deveRetornarUmToken() {
		
		UsuarioForm usuario = new UsuarioForm("aluno@email.com", "123456");
		
		given()
			.contentType("application/json")
			.accept("application/json")
			.body(usuario)
		.expect()
		.statusCode(200)
		.when()
			.post("/auth")
		.then()
			.body("token", notNullValue())
			.and()
		.body("string", equalTo("Bearer"));
	}
	
	@Test
	public void deveRetornarErroDeLogin() {
		
		UsuarioForm usuario = new UsuarioForm("abc@email.com", "abc");
		
		given()
			.contentType("application/json")
			.accept("application/json")
			.body(usuario)
		.expect()
			.statusCode(400)
		.when()
			.post("/auth")
		;
	}
}
